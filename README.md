# Pandoc Markdown

Ce dépôt contient une brève présentation sur les avantages de [Markdown] et
[Pandoc] dans le fichier [pandoc_markdown.md](pandoc_markdown.md). Ce même
fichier est utilisé comme source pour la conversion avec [Pandoc] dans
différents formats.

## Utilisation

`make all` génère environ 25 fichiers, groupés en 4 catégories :

   * pdf
   * html
   * slides
   * doc

permettant de voir le rendu de la conversion du fichier 
[pandoc_markdown.md](pandoc_markdown.md) avec différentes options [Pandoc].

## Notes

La génération des slides avec [reveal.js] nécessite [Pandoc] v2.9.2.

La génération des PDF et des slides Beamer nécessite l'installation de [LaTeX],
et du moteur `lualatex`.

[//]: # (Reference and links)
  [Markdown]: https://daringfireball.net/projects/markdown/
  [Pandoc]: https://pandoc.org
  [reveal.js]: https://revealjs.com/
  [LaTeX]: https://www.latex-project.org/