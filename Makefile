# Project's Makefile
MAKEFLAGS += --no-print-directory

.PHONY: all

all: clean pdf html slides doc themes
pdf: pandoc_markdown_simple.pdf pandoc_markdown.pdf
html: pandoc_markdown.html pandoc_markdown.htm
slides: pandoc_markdown_beamer.pdf pandoc_markdown_slidy.html pandoc_markdown_remark.html pandoc_markdown_dzslides.html pandoc_markdown_s5.html pandoc_markdown_slideous.html pandoc_markdown_reveal.html pandoc_markdown_reveal2.html
doc: pandoc_markdown.docx pandoc_markdown.odt pandoc_markdown.pptx

themes:
	@$(MAKE) pandoc_markdown_reveal_theme theme=simple
	@$(MAKE) pandoc_markdown_reveal_theme theme=beige
	@$(MAKE) pandoc_markdown_reveal_theme theme=black
	@$(MAKE) pandoc_markdown_reveal_theme theme=blood
	@$(MAKE) pandoc_markdown_reveal_theme theme=league
	@$(MAKE) pandoc_markdown_reveal_theme theme=moon
	@$(MAKE) pandoc_markdown_reveal_theme theme=night
	@$(MAKE) pandoc_markdown_reveal_theme theme=serif
	@$(MAKE) pandoc_markdown_reveal_theme theme=sky
	@$(MAKE) pandoc_markdown_reveal_theme theme=solarized
	@$(MAKE) pandoc_markdown_reveal_theme theme=white

# clean
clean:
	@rm -f *~ pandoc_markdown*.pdf *.htm* *.docx *.odt *.pptx *.tar.gz.*

# Pandoc PDF (simple)
pandoc_markdown_simple.pdf: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc pandoc_markdown.md -o pandoc_markdown_simple.pdf

# Pandoc PDF (elegant)
pandoc_markdown.pdf: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc \
		--variable mainfont="DejaVu Sans" \
		--variable monofont="DejaVu Sans Mono" \
		--variable fontsize=11pt \
		--variable geometry:"top=1.5cm, bottom=2.5cm, left=1.5cm, right=1.5cm" \
		--variable geometry:a4paper \
		--variable colorlinks \
		--variable linkcolor=blue \
		--variable urlcolor=blue \
		--table-of-contents \
		--number-sections \
		-f markdown $< \
		--pdf-engine=lualatex \
		-o $@

# HTML
pandoc_markdown.htm: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -t html4 -o $@

# HTML5
pandoc_markdown.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -t html5 -o $@

# Using Pandoc Beamer (pdf)
# https://pandoc.org/MANUAL.html#variables-for-beamer-slides
# Another theme: theme:Madrid
pandoc_markdown_beamer.pdf: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -r markdown -t beamer \
		--slide-level=2 \
		--toc \
		--variable aspectratio=169 \
		--variable navigation=horizontal \
		--highlight-style=tango \
		--variable theme:Warsaw \
		--variable colorlinks \
		--variable linkcolor=blue \
		--variable urlcolor=blue \
		-o $@

# All HTML Slides
## Using Pandoc Slideous
pandoc_markdown_slideous.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -s -t slideous -o $@

## Using Pandoc Slidy2
pandoc_markdown_slidy.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -s -t slidy -o $@

## Using remark.js
pandoc_markdown_remark.html: remark.template pandoc_markdown.md
	@echo -- Creating $@ --
	@perl -p -e 's/MY_CONTENT_GOES_HERE/qx(cat pandoc_markdown.md)/ge'< remark.template > pandoc_markdown_remark.html

## Using Pandoc DZslides
pandoc_markdown_dzslides.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -s -t dzslides -o $@

## Using Pandoc S5
pandoc_markdown_s5.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -s -t s5 -o $@

## Using Pandoc reveal.js
pandoc_markdown_reveal.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@$(MAKE) download_revealjs
	@pandoc $< \
		--standalone \
		--self-contained \
		--slide-level=2 \
		-t revealjs \
		--variable revealjs-url=./reveal.js \
		--variable  slideNumber=true   \
		--variable     progress=true   \
		--variable        width=1800   \
		--variable       height=1012   \
		--variable        theme=simple \
		-o $@

# beige, black, blood, league, moon, night, serif, simple, sky, solarized, white
pandoc_markdown_reveal_theme: pandoc_markdown.md
	@echo -- Creating $@: $(theme) --
	@pandoc $< \
		--standalone \
		--self-contained \
		--slide-level=2 \
		-t revealjs \
		--variable revealjs-url=./reveal.js \
		--variable  slideNumber=true   \
		--variable     progress=true   \
		--variable        width=1800   \
		--variable       height=1012   \
		--variable        theme=$(theme) \
		-o $@_$(theme).html

pandoc_markdown_reveal2.html: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< --self-contained \
		-t revealjs \
		--variable revealjs-url="https://cdnjs.cloudflare.com/ajax/libs/reveal.js/4.0.1/" \
		-o $@

pandoc_markdown.docx: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -t docx -o $@

pandoc_markdown.odt: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -t odt -o $@

pandoc_markdown.pptx: pandoc_markdown.md
	@echo -- Creating $@ --
	@pandoc $< -t pptx -o $@

# Testing the raw HTML output of pandoc
test:
	pandoc pandoc_markdown.md -o output.html

download_revealjs:
	@echo -- Downloading reveal.js --
	@rm -rf reveal.js || true
	@rm -rf master.tar.gz || true
	@wget --quiet https://github.com/hakimel/reveal.js/archive/master.tar.gz
	@tar -xzf master.tar.gz
	@mv reveal.js-master reveal.js
	@rm master.tar.gz

prez1:
	firefox -new-window pandoc_markdown_reveal.html

prez2:
	impressive -i 18 -q pandoc_markdown_beamer.pdf
