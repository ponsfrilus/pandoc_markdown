---
author: Nicolas Borboën <<nicolas.borboen@epfl.ch>>
title: Markdown & Pandoc
subtitle: Une brève présentation sur leurs avantages.
date: 2020-07-02
---

# Markdown & Pandoc

Une brève présentation sur les avantages de [Markdown] et [Pandoc].

---

# Markdown

![](./img/Markdown600.png)

---

## À propos

[Markdown] est un [langage de balisage
léger](https://en.wikipedia.org/wiki/Lightweight_markup_language) qui permet
d’écrire simplement du texte et de le convertir en code HTML pour sa publication
sur un site web.

---

## Facile à lire, facile à écrire

   * c’est une syntaxe de formatage légère "plain text"
   * c’est un programe écrit en [Perl] permettant la conversion vers l’HTML

Site officiel : [https://daringfireball.net/projects/markdown/](https://daringfireball.net/projects/markdown/)

---

## Historique

[Markdown] a été développé par [John
Gruber](https://en.wikipedia.org/wiki/John_Gruber) (en collaboration avec [Aaron
Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz) pour la syntaxe).

La première version date du **19 mars 2004** !

[Markdown] est un logiciel libre, distribué sous les termes d’une licence de type
BSD ([voir la licence](https://daringfireball.net/projects/markdown/license)).

---

## Markdown goal

L’objectif principal de la syntaxe de formatage de [Markdown] est de la rendre
aussi **lisible que possible**.

L’idée est qu’un document au format Markdown doit être **publiable tel quel,
en texte brut**, sans avoir l’air d’être marqué avec des balises ou des
instructions de formatage.

Alors que la syntaxe de Markdown a été influencée par plusieurs filtres
texte-HTML existants, la plus grande source d’inspiration pour la syntaxe de
Markdown est le **format des e-mails en texte brut**.

<small>Voir le site [useplaintext.email](https://useplaintext.email/)</small>

---

## Markdown standardisation

Markdown a initialement été caractérisé par une spécification informelle.

Au fil du temps, de nombreuses implémentations Markdown sont apparues. Les
gens les ont développées principalement en raison du besoin de fonctionnalités
supplémentaires comme les tableaux. Cependant, Gruber a soutenu qu’une
normalisation complète serait erronée :

> Différents sites (et personnes) ont des besoins différents. Aucune syntaxe ne
> ferait le bonheur de tous.
> — John Gruber (source: [Markdown wikipedia])

Néanmoins, les [RFC 7763] et [RFC 7764] sont introduites en mars 2016.

---

## Markdown flavors

Parmis toutes les variantes de [Markdown] qui ont vu le jour, les deux les plus
importantes sont :

   * **CommonMark** — [https://commonmark.org/](https://commonmark.org/)  
     Un effort de normalisation provenant des cérateurs de Pandoc en 2012.
     CommonMark est le MarkDown de référence pour Gitlab.com

   * **GitHub Flavored Markdown** (GFM) — [https://github.github.com/gfm/](https://github.github.com/gfm/)  
     En 2017, GitHub a publié une spécification formelle de leur GitHub Flavored
     Markdown (GFM) basée sur CommonMark. Il s’agit d’un sur-ensemble strict de
     CommonMark, suivant exactement ses spécifications, à l’exception des
     tables, des barrés, des liens automatiques et des listes de tâches, que GFM
     ajoute comme extensions.

---

## Syntaxe de base

<small>Voir la documentation officielle de [Markdown].</small>
```MarkDown

# Titre 1
## Titre 2
...etc

1. Liste numérotée 1
1. Liste numérotée 2
1. Liste numérotée 3

*, +, - pour les listes non-numérotées.

[Titre du lien](http://url_du_lien.com)

**Texte en gras**, _texte en italique_

`code`
```

---

## Références Markdown

Il est possible d’utiliser des références dans le texte, par exemple [EPFL].

En bas de document utiliser:

```markdown
[//]: # (Reference and links)
 [EPFL]: http://www.epfl.ch
```

---

## Commentaires Markdown

C’est expérimental ! La référence doit être unique et non utilisée :

```markdown
[--]: # Mon commentaire
```

---

## Utilisation en ligne de commande (markdown.pl)

```bash
echo "A **simple** test" | markdown
<p>A <strong>simple</strong> test</p>
```

# Pandoc

![](./img/pandoc.png)

---

## À propos

[Pandoc] est un logiciel de conversion de documents en ligne de commande
développé par [John MacFarlane](https://johnmacfarlane.net/) en [Haskell] et
publié sous licence GPL, depuis 2006.

Il permet de convertir des documents en Markdown, reStructuredText, Textile,
HTML, DocBook, LaTeX, syntaxe MediaWiki, syntaxe TWiki, OPML, Org-mode,
Txt2Tags, Microsoft Word docx, LibreOffice ODT, EPUB ou Haddock markup vers des
formats HTML, Microsoft Word, PDF...

---

## Input vs Output

`pandoc --list-input-formats`

vs

`pandoc --list-output-formats`

---

## Pandoc convertion graph

<small>(souce: [https://pandoc.org/diagram.jpg](https://pandoc.org/diagram.jpg))</small>

![Pandoc convertion graph](img/diagram.jpg)

---

# Utilisation

Les sections suivantes présentent les commandes de conversion de base.

---

## HTML

Pour convertir un document `.md` en `.html` :

* Avec Markdown uniquement :  
`markdown pandoc_markdown.md > pandoc_markdown.html`

* Avec Pandoc :  
`pandoc pandoc_markdown.md -o pandoc_markdown.html`

---

## PDF

Pour convertir un document `.md` en `.pdf` :

* Basic :  
`pandoc pandoc_markdown.md -o pandoc_markdown.pdf`

* Avec table des matières :  
`pandoc --toc pandoc_markdown.md -o pandoc_markdown.pdf`

---

## Avec titre, auteur et date (pandoc)

Ajouter dans le document Markdown les entrées suivantes au début du fichier :

```pandoc
% — EPFL — \
  Markdown & Pandoc
% Nicolas Borboën <nicolas.borboen@epfl.ch>
% 2016-05-13
```

Ceci utilise l’extension [pandoc_title_block](https://pandoc.org/MANUAL.html#extension-pandoc_title_block).

---

## Avec titre, auteur et date (yaml)

Une alternative est l’utilisation d’un bloque `yaml` :

```yaml
title: Markdown & Pandoc
subtitle: Une brève présentation sur leurs avantages.
author: Nicolas Borboën
date: 2020 onwards
theme: Madrid
colortheme: dolphin
fontsize: 10pt
```

Ceci utilise l’extension [yaml_metadata_block](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block).

---

## Présentations

Il est possible de convertir un fichier Markdown en présentation. Différents 
plugins sont disponibles.

* Classe [Beamer](https://ctan.org/pkg/beamer) de [LaTeX] :  
```bash
pandoc -r markdown pandoc_markdown.md -t beamer \
		-o pandoc_markdown_slides.pdf --slide-level=1 \
		--toc --highlight-style=tango --variables theme:Warsaw
```

* [slideous], [slidy], [dzslides], [S5], [revealjs] :  
```bash
pandoc -s pandoc_markdown.md -t XXXX \
		-o pandoc_markdown_XXXX.html
```

---

## Documents

Un fichier Markdown peut également être convertit en `*.docx` ou `*.odt` :

---

### Office Open XML
```bash
pandoc pandoc_markdown.md -t docx \
		o pandoc_markdown.doc
```

---

### Open Document Format for Office Applications (ODF)
```bash
pandoc pandoc_markdown.md -t odt \
		-o pandoc_markdown.odt
```

---

# Démonstration

  * [PDF](./pandoc_markdown.pdf)
  * [LaTeX Beamer](./pandoc_markdown_beamer.pdf)
     * [Impressive!](http://impressive.sourceforge.net/)
        * `impressive pandoc_markdown_beamer.pdf` <kbd>Tab</kbd>, <kbd>Enter</kbd>, ...
  * [Reveal.js](./pandoc_markdown_reveal.html)
  * [Slidy](./pandoc_markdown_slidy.html)
  * [ODT](./pandoc_markdown.odt)
  * [HTML](./pandoc_markdown.html)

---

## Code

Le code de cette présentation se trouve sur https://gitlab.com/ponsfrilus/pandoc_markdown.

[Pandoc] v2.9.2 est requis. Pour les convertions utilisant LaTeX il est
nécessaire d’avoir LaTeX installé et utiliser le moteur lualatex (ou changer le
[Makefile](./Makefile) pour utiliser d’autres moteurs).

1. `git clone git@gitlab.com:ponsfrilus/pandoc_markdown.git && cd pandoc_markdown`
2. `make all`

---

# Liens

* Sites officiels
    * [Markdown](https://daringfireball.net/projects/markdown/)
    * [Pandoc](http://pandoc.org/)
    * [Pandoc Manual](https://pandoc.org/MANUAL.pdf)
* EPFL
    * [Création et conversion de documents avec Pandoc](http://flashinformatique.epfl.ch/spip.php?article2658)
    * [Markdown](http://flashinformatique.epfl.ch/spip.php?article2629)
    * [Élaboration et conversion de documents avec Markdown et Pandoc](http://enacit1.epfl.ch/markdown-pandoc/)
* Markdown
    * [Mardown CheatSheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



[//]: # (Reference and links)
  [Markdown]: https://daringfireball.net/projects/markdown/
  [Markdown wikipedia]: https://en.wikipedia.org/wiki/Markdown
  [Pandoc]: https://pandoc.org
  [Pandoc wikipedia]: https://fr.wikipedia.org/wiki/Pandoc
  [Using pandoc to produce reveal.js slides]: https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides
  [EPFL]: http://www.epfl.ch
  [RFC 7763]: https://tools.ietf.org/html/rfc7763
  [RFC 7764]: https://tools.ietf.org/html/rfc7764
  [slideous]: https://goessner.net/articles/slideous/
  [slidy]: https://www.w3.org/Talks/Tools/Slidy2
  [dzslides]: http://paulrouget.com/dzslides/
  [S5]: https://meyerweb.com/eric/tools/s5
  [revealjs]: https://revealjs.com/
  [LaTeX]: https://www.latex-project.org/
  [Perl]: https://www.perl.org/
  [Haskell]: https://www.haskell.org/
